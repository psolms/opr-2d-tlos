# OPR Non Binary Elevation

## The Goal
Traditionally, OPR is played on a 2D plane. This document is an attempt to add a third dimension, without taking away from the core experience. These rules are meant to be compatible with both GDF and GDF:FF.

### Setup
After you have set up your terrain, starting with ground level as level 0, assign a height level or "plane" to each piece of terrain, or significant landing space. Both players should agree to the vertical planes. As a general guideline, use the following measurements:

- 0-2": Level 0
- 2.1"-5": Level 1
- 5.1"-8": Level 2
- 8.1"-11": Level 3

All that matters is that the measurements are consistent. Consider marking each elevated level with a number token or dice to keep it consistent through the match.

### Traversal
Increasing elevation is done by climbing. One level of elevation can be gained by spending 3" of movement.

Decreasing elevation can be done by climbing for the same movement cost, or by jumping as per the standard rules. The only addendum is to change every instance of "3" to "one elevation level".

A melee or charge against a model at a higher elevation cannot be completed unless the charging unit has enough movement to get to the target, and space on the elevation to stay.

### Measuring LOS Across Planes
Before the match begins, agree with your opponents which method to use. They are incompatible with each other, and only one or the other should be used in any given game.When measuring for LOS between two points on different planes, apply the rule adjustments based on the difference in plane: (ie: level 3 shooting at level 1 is a difference of 2).

#### Simple
The simple approach is meant to provide basic rulings. 

- 0-1 - no LOS blocking or cover granted from elevation alone
- 2 - cover terrain from below, ignores cover from above
- 3+ - blocks LOS from below, ignores cover and +1 to hit from above

#### Advanced
For a more 'realistic' approach, the effects are shifted in favor of higher ground, and range becomes a consideration.

- If the activated unit is on a higher plane relative to its target:
    - for every elevation level difference past the first, add 1" to the ranged weapon
    - an elevation difference of 2 or more ignores cover terrain
    - an elevation difference of 3 or more ignores LOS blocking terrain for anything other than other elevation terrain
- If the activated unit is on a lower plane relative to its target:
    - for every elevation level difference past the first, subtract 2" from the ranged weapon
    - an elevation difference of 1 grants cover terrain for ranged weapons
    - an elevation difference of 2 is LOS blocking terrain

##### Examples
- Unit A is on Level 4 elevation. He targets unit B on level 2 elevation. Since the elevation difference is 2, the cover that would normally be granted to unit B is ignored.
- Unit C is on level 0 elevation, behind a piece of LOS blocking terrain. Unit A, being 4 levels higher, could target him, ignoring cover and LOS blocking.
- Unit D is on level 0 elevation on the ground floor beside the building unit B is on. Because the terrain blocking LOS is from elevation terrain, Unit A cannot target him.

## More Ideas (flesh these out)
The benefit of this type of plane sorting could also include variable terrain. For instance, a terrain feature with a special rule that it changes elevation each round.